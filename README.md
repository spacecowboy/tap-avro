# tap-avro

`tap-avro` is a Singer tap for Avro.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.

Heavily borrowed code from [tap-csv](https://github.com/MeltanoLabs/tap-csv).

## Capabilities

* `sync`
* `catalog`
* `discover`

Note: This tap currently does not support incremental state.

## Settings

| Setting             | Required | Default | Description |
|:--------------------|:--------:|:-------:|:------------|
| files               | False    | None    | An array of avro file stream settings. |
| avro_files_definition| False    | None    | A path to the JSON file holding an array of file settings. |

A full list of supported settings and capabilities is available by running: `tap-avro --about`

The `config.json` contains an array called `files` that consists of dictionary objects detailing each destination table to be passed to Singer. Each of those entries contains: 
* `entity`: The entity name to be passed to singer (i.e. the table)
* `path`: Local path to the file to be ingested. Note that this may be a directory, in which case all files in that directory and any of its subdirectories will be recursively processed. All files in the directory must share the same schema.
* `keys`: The names of the columns that constitute the unique keys for that entity

Example:

```json
{
	"files":	[ 	
					{	"entity" : "leads",
						"path" : "/path/to/leads.avro",
						"keys" : ["Id"]
					},
					{	"entity" : "opportunities",
						"path" : "/path/to/opportunities.avro",
						"keys" : ["Id"]
					}
				]
}
```

Optionally, the files definition can be provided by an external json file:

**config.json**
```json
{
	"avro_files_definition": "files_def.json"
}
```


**files_def.json**
```json
[ 	
	{	"entity" : "leads",
		"path" : "/path/to/leads.avro",
		"keys" : ["Id"]
	},
	{	"entity" : "opportunities",
		"path" : "/path/to/opportunities.avro",
		"keys" : ["Id"]
	}
]
```

## Installation

```bash
pipx install git+https://gitlab.com/spacecowboy/tap-avro.git
```

## Usage

You can easily run `tap-avro` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
tap-avro --version
tap-avro --help
tap-avro --config CONFIG --discover > ./catalog.json
```

## Developer Resources

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `tap_avro/tests` subfolder and
  then run:

```bash
poetry run pytest --doctest-modules
```

You can also test the `tap-avro` CLI interface directly using `poetry run`:

```bash
poetry run tap-avro --help
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

Your project comes with a custom `meltano.yml` project file already created. Open the `meltano.yml` and follow any _"TODO"_ items listed in
the file.

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-avro
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-avro --version
# OR run a test `elt` pipeline:
meltano elt tap-avro target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to 
develop your own taps and targets.
