"""
Some Avro specific functions
"""


from collections.abc import Mapping
import datetime
import json
from fastavro import reader


def read_avroschema(filepath: str) -> dict:
    with open(filepath, 'rb') as fo:
        return get_schema_from_reader(reader(fo))


def get_schema_from_reader(reader) -> dict:
    return json.loads(reader.metadata.get('avro.schema'))


def avro_to_jsonschema_type(avrotype: list) -> list:
    """
    Converts an avro type to jsonschema type.
    Input and output is always a list of possible types, even if there is only one type.

    >>> avro_to_jsonschema_type(["string", "boolean", "int", "long", "float", "double", "null"])
    ['string', 'boolean', 'number', 'number', 'number', 'number']

    >>> avro_to_jsonschema_type(["null", {"type": "int", "logicalType": "time-millis"}])
    [{'type': 'string', 'format': 'time'}]

    >>> avro_to_jsonschema_type(["null", {"type": "int", "logicalType": "date"}])
    [{'type': 'string', 'format': 'date'}]

    >>> avro_to_jsonschema_type(["null", {"type": "long", "logicalType": "timestamp-millis"}])
    [{'type': 'string', 'format': 'date-time'}]
    """

    jstype = []

    for avtype in avrotype:
        if avtype == "null":
            # Handled by optional flag instead
            pass
        elif avtype == "boolean":
            jstype.append("boolean")
        elif avtype == "int" or avtype == "long" or avtype == "float" or avtype == "double":
            jstype.append("number")
        elif avtype == "bytes":
            raise NotImplementedError("bytes is not implemented")
        elif avtype == "string":
            jstype.append("string")
        elif isinstance(avtype, Mapping):
            if 'logicalType' not in avtype:
                raise NotImplementedError("Can't handle maps yet")
            
            logical_type = avtype['logicalType']

            if logical_type == "date":
                jstype.append({'type': 'string', 'format': 'date'})
            elif logical_type == "time-millis" or logical_type == "time-micros":
                jstype.append({'type': 'string', 'format': 'time'})
            elif logical_type == "timestamp-millis" or logical_type == "timestamp-micros":
                jstype.append({'type': 'string', 'format': 'date-time'})
            else:
                raise NotImplementedError(f"Can't handle logicalType: {logical_type}")

    return jstype


def avro_record_to_jsonschema_record(avrorecord):
    """
    Converts an avro record into a jsonschema record for the given schema.

    >>> avro_record_to_jsonschema_record({'id': 5})
    {'id': 5}

    >>> avro_record_to_jsonschema_record({'d': datetime.date.fromisoformat('2019-12-04')})
    {'d': '2019-12-04'}

    >>> avro_record_to_jsonschema_record({'t': datetime.time.fromisoformat('04:23:01.000384')})
    {'t': '04:23:01.000384'}

    >>> avro_record_to_jsonschema_record({'ts': datetime.datetime.fromisoformat('1970-01-01T00:00:00.005000+00:00')})
    {'ts': '1970-01-01T00:00:00.005000+00:00'}
    """
    result = {}

    for k, v in avrorecord.items():
        # date and time formats need to be handled
        if isinstance(v, datetime.time) or isinstance(v, datetime.date) or isinstance(v, datetime.datetime):
            result[k] = v.isoformat()
        else:
            result[k] = v

    return result
