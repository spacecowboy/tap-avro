"""Custom client handling, including AvroStream base class."""

import os
from fastavro import reader
import json
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th
from singer_sdk.streams import Stream

from .singer import SingerTypeHelper
from .avro import (
    avro_to_jsonschema_type,
    read_avroschema,
    avro_record_to_jsonschema_record
)


class AvroStream(Stream):
    """Stream class for Avro streams."""

    def __init__(self, *args, **kwargs):
        """Init AvroStream."""
        # cache file_config so we dont need to go iterating the config list again later
        self.file_config = kwargs.pop("file_config")
        self.file_paths = []
        super().__init__(*args, **kwargs)

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """
        for file_path in self.get_file_paths():
            with open(file_path, 'rb') as fo:
                for record in reader(fo):
                    yield avro_record_to_jsonschema_record(record)
    
    def get_file_paths(self) -> list:
        """Return a list of file paths to read.
        This tap accepts file names and directories so it will detect
        directories and iterate files inside.
        """
        # Cache file paths so we dont have to iterate multiple times
        if self.file_paths:
            return self.file_paths

        file_path = self.file_config["path"]
        if not os.path.exists(file_path):
            raise Exception(f"File path does not exist {file_path}")

        file_paths = []
        if os.path.isdir(file_path):
            clean_file_path = os.path.normpath(file_path) + os.sep
            for filename in os.listdir(clean_file_path):
                file_path = clean_file_path + filename
                if self.is_valid_filename(file_path):
                    file_paths.append(file_path)
        else:
            if self.is_valid_filename(file_path):
                file_paths.append(file_path)

        if not file_paths:
            raise Exception(
                f"Stream '{self.name}' has no acceptable files. \
                    See warning for more detail."
            )
        self.file_paths = file_paths
        return file_paths

    def is_valid_filename(self, file_path: str) -> bool:
        """Return a boolean of whether the file includes Avro extension."""
        is_valid = True
        if os.path.splitext(file_path)[1].lower() != ".avro":
            is_valid = False
            self.logger.warning(f"Skipping non-avro file '{file_path}'")
            self.logger.warning(
                "Please provide an Avro file that ends with '.avro'; e.g. 'users.avro'"
            )
        return is_valid
    
    @property
    def schema(self) -> dict:
        """Return dictionary of record schema.
        Dynamically detect the json schema for the stream.
        This is evaluated prior to any records being retrieved.
        """
        properties: List[th.Property] = []
        self.primary_keys = self.file_config.get("keys", [])
        
        schema = read_avroschema(self.get_file_paths()[0])
        
        for field in schema['fields']:
            properties.append(
                th.Property(
                    name=field['name'],
                    wrapped=SingerTypeHelper({
                        'type': avro_to_jsonschema_type(field['type'])
                    }),
                    required="null" not in field['type']
                )
            )
        return th.PropertiesList(*properties).to_dict()
