"""
Some singer utilities
"""

class SingerTypeHelper:
    """
    A class like JSONTypeHelper except the type is provided dynamically
    """
    
    def __init__(self, type_dict) -> None:
        self.type_dict = type_dict

    def to_dict(self) -> dict:
        return self.type_dict
