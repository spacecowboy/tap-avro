"""Avro tap class."""

import json
import os
from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th
from singer_sdk.helpers._classproperty import classproperty

from tap_avro.streams import AvroStream

class TapAvro(Tap):
    """Avro tap class."""
    name = "tap-avro"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "files",
            th.ArrayType(
                th.ObjectType(
                    th.Property("entity", th.StringType, required=True),
                    th.Property("path", th.StringType, required=True),
                    th.Property("keys", th.ArrayType(th.StringType), required=True),
                )
            ),
            description="An array of avro file stream settings.",
        ),
        th.Property(
            "avro_files_definition",
            th.StringType,
            description="A path to the JSON file holding an array of file settings.",
        ),
    ).to_dict()

    @classproperty
    def capabilities(self) -> List[str]:
        """Get tap capabilites."""
        return ["sync", "catalog", "discover"]

    def get_file_configs(self) -> List[dict]:
        """Return a list of file configs.

        Either directly from the config.json or in an external file
        defined by avro_files_definition.
        """
        avro_files = self.config.get("files")
        avro_files_definition = self.config.get("avro_files_definition")
        if avro_files_definition:
            if os.path.isfile(avro_files_definition):
                with open(avro_files_definition, "r") as f:
                    avro_files = json.load(f)
            else:
                self.logger.error(f"tap-avro: '{avro_files_definition}' file not found")
                exit(1)
        if not avro_files:
            self.logger.error("No avro file definitions found.")
            exit(1)
        return avro_files

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [
            AvroStream(
                tap=self,
                name=file_config.get("entity"),
                file_config=file_config,
            )
            for file_config in self.get_file_configs()
        ]


if __name__ == "__main__":
    TapAvro.cli()
