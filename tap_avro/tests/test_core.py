"""Tests standard tap features using the built-in SDK tests library."""

import os

from singer_sdk.testing import get_standard_tap_tests

from tap_avro.tap import TapAvro


SAMPLE_CONFIG = {
    "files": [
        {
            "entity": "testfile",
            "path": os.path.join(os.path.dirname(__file__), "test.avro"),
            "keys": [
                "id"
            ]
        }
    ]
}


# def write_test_avro():
#     schema = {
#         "namespace": "fmcsa.avro",
#         "type": "record",
#         "name": "testavro",
#         "fields": [
#             {
#                 "name": "id",
#                 "type": ["long", "null"]
#             },
#             {
#                 "name": "name",
#                 "type": ["string", "null"]
#             },
#             {
#                 "name": "age",
#                 "type": ["int", "null"]
#             },
#             {
#                 "name": "date",
#                 "type": [
#                     {
#                         "type": "int",
#                         "logicalType": "date"
#                     }, 
#                     "null"
#                 ]
#             },
#             {
#                 "name": "time",
#                 "type": [
#                     {
#                         "type": "int",
#                         "logicalType": "time-millis"
#                     }, 
#                     "null"
#                 ]
#             },
#             {
#                 "name": "timestamp",
#                 "type": [
#                     {
#                         "type": "long",
#                         "logicalType": "timestamp-millis"
#                     }, 
#                     "null"
#                 ]
#             }
#         ]
#     }

#     parsed_schema = parse_schema(schema)

#     records = [
#         dict(
#             id=1,
#             name="Alice",
#             age=35,
#             date=6734,
#             time=3601000,
#             timestamp=1648547368000
#         )
#     ]

#     with open(os.path.join(os.path.dirname(__file__), 'test.avro'), 'wb') as fo:
#         writer(fo, parsed_schema, records)


# Run standard built-in tap tests from the SDK:
def test_standard_tap_tests():
    """Run standard tap tests from the SDK."""
    print(f"I am here {os.path.join(os.path.dirname(__file__), 'test.avro')}")
    tests = get_standard_tap_tests(
        TapAvro,
        config=SAMPLE_CONFIG
    )
    for test in tests:
        test()
